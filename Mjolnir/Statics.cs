﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mjolnir
{
    public static class Statics
    {
        /// <summary> Flag stating if the engine is in debug mode </summary>
        public static Boolean Debug = true;

        public static String TempMessage = "Hello World!";
    }
}
